﻿using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class PlayerController : MonoBehaviour {

	public float moveSpeed = 10f;
	//ADD
	[HideInInspector]
	public Vector3 velocity;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		float moveHorizontal = Input.GetAxis ("Horizontal");
		float moveVertical = Input.GetAxis ("Vertical");
		
		//ADD
		velocity = new Vector3(moveHorizontal * moveSpeed * Time.deltaTime, moveVertical * moveSpeed * Time.deltaTime, transform.position.z);

		//Modified
		transform.Translate (velocity);
	}
}

//ADD
[CustomEditor(typeof(PlayerController))]
public class PlayerControllerEditor : Editor {

	PlayerController pc;

	void OnEnable() {
		pc = (PlayerController)target;
	}

	public override void OnInspectorGUI()
	{
		base.OnInspectorGUI();
		EditorGUILayout.HelpBox("Challenge : Buat agar ketika input horizontal dan vertical ditekan bersamaan akan menghasil kecepatan yang sama dengan satu inputan", MessageType.Info);
		GUI.enabled = false;
		EditorGUILayout.FloatField("Current Velocity X", pc.velocity.x);
		EditorGUILayout.FloatField("Current Velocity Y", pc.velocity.y);
		EditorGUILayout.FloatField("Rata Velocity", pc.velocity.magnitude);
		GUI.enabled = true;
	}
}